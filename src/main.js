// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { sync } from 'vuex-router-sync'

Vue.config.productionTip = false

// load external libraries

// load moment
import moment from 'moment'
Object.defineProperty(Vue.prototype, '$moment', { value: moment })

// load firebase application instance
import firebaseApp from '@/utils/firebase.js'
Object.defineProperty(Vue.prototype, '$firebaseApp', { value: firebaseApp })
Object.defineProperty(Vue.prototype, '$firestoreDB', { value: firebaseApp.firestore() })

// load firebase
import * as firebase from 'firebase'
Object.defineProperty(Vue.prototype, '$firebase', { value: firebase })

// load auth service
import authService from '@/services/authService.js'
Object.defineProperty(Vue.prototype, '$authService', { value: authService })

// load lodash
import _ from 'lodash'
Object.defineProperty(Vue.prototype, '$_', { value: _ })

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import store from './store'
sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
