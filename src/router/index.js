import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Admin from '@/components/admin/Index'
import CourseManager from '@/components/admin/CourseManager'
import Login from '@/components/Login'
import authService from '../services/authService'
import firebaseApp from '../utils/firebase.js'
import _ from 'lodash'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter: (to, from, next) => {
        authService.onAuthStateChanged().then(r => {
          next({ name: 'Admin' })
        }).catch(e => {
          next()
        })
      }
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      beforeEnter: (to, from, next) => {
        firebaseApp.auth().onAuthStateChanged(user => {
          if (_.isNull(user)) {
            next('/login')
          } else {
            next()
          }
        })
      },
      children: [
        {
          path: 'course-manager',
          name: 'AdminCourseManager',
          component: CourseManager
        }
      ]
    }
  ]
})