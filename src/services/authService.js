import firebaseApp from '@/utils/firebase.js'
import * as firebase from 'firebase'

let authService = {
    onAuthStateChanged() {
        return new Promise((resolve, reject) => {
            // this adds a hook for the initial auth-change event
            firebaseApp.auth().onAuthStateChanged(user => {
                if (user) {
                    resolve(user)
                } else {
                    reject("Unauthenticated")
                }

            })
        })
    },
    currentUser() {
        let user = firebase.auth().currentUser
        if (user) {
            return user
        }
        return false
    },
    loginWithEmailAndPassword(email, password) {
        return firebaseApp.auth().signInWithEmailAndPassword(email, password)
    },
    loginWithGoogle() {
        return firebaseApp.auth().signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(function (result) {
        })
    },
    logout() {
        firebaseApp.auth().signOut().then(() => {

        })
    }
}

export default authService