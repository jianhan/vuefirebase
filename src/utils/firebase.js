// load firebase
import * as firebase from 'firebase'
import { firebaseConfigs } from '../../.env'
require("firebase/firestore")

// initialize firebase
const firebaseApp = firebase.initializeApp(firebaseConfigs)
export default firebaseApp