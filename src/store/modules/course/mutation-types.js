export default [
  'resetState',
  'setCourseRef',
].reduce((acc, value) => {
  acc[value] = value
  return acc
}, {})
