import types from './mutation-types'
import { firebaseAction } from 'vuexfire'

export default {
  resetState({ commit }) {
    commit(types.resetState)
  },
  setCourseRef: firebaseAction(({ bindFirebaseRef, unbindFirebaseRef }, ref) => {
    bindFirebaseRef('courses', ref)
  })
}
